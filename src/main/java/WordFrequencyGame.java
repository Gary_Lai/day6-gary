import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String SPLIT_REGEX = "\\s+";

    public String getWordFrequencyFormat(String inputStr) {
        String[] strListBySplit = inputStr.split(SPLIT_REGEX);

        Map<String, Integer> wordFrequencyCounted = countWordFrequency(strListBySplit);
        List<Map.Entry<String, Integer>> sortedStrCountList = wordFrequencyCounted.entrySet().stream()
                .sorted((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()))
                .collect(Collectors.toList());

        return formatOutput(sortedStrCountList);
    }

    private static Map<String, Integer> countWordFrequency(String[] strListBySplit) {
        Map<String, Integer> strCountMap = new HashMap<>();
        Arrays.stream(strListBySplit).forEach((str) ->
                strCountMap.put(str, strCountMap.getOrDefault(str, 0) + 1));
        return strCountMap;
    }

    private String formatOutput(List<Map.Entry<String, Integer>> sortedStrCountList) {
        StringJoiner outputJoiner = new StringJoiner("\n");
        sortedStrCountList.stream().map(entry -> entry.getKey() + " " + entry.getValue()).forEach(outputJoiner::add);
        return outputJoiner.toString();
    }

}
