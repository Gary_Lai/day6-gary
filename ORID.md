##  Daily Summary 

**Time: 2023/7/17**

**Author: 赖世龙**

---

**O (Objective):**    Today, I learned about refactoring and code smell. Through the teacher's explanation and assigned exercises

**R (Reflective):**  Satisfied

**I (Interpretive):**  I deeply understand that it is not enough to just write the right code. What is more important is to improve the quality of code through refactoring to avoid code smell

**D (Decisional):**  In the future code writing, keep TDD in mind, not only to complete the test and code, but also to refactor the code to improve the quality and readability of the code

